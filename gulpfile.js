var gulp = require('gulp'),
    compass = require('gulp-compass'),
    spritesmith = require('gulp.spritesmith');

gulp.task('default', function() {

});

gulp.task('compass', function() {
    gulp.src('./scss/**/*.scss')
        .pipe(compass({
            css: 'css',
            sass: './scss',
            image: 'img',
            style: 'compressed'
            //style: 'expanded'
        }))
        .on('error', function(error) {
            // Would like to catch the error here
            console.log(error);
            this.emit('end');
        })
        .pipe(gulp.dest('css'));
});

gulp.task('sprite', function () {
    var spriteData = gulp.src('./img/images/*.png')
        .pipe(spritesmith({
            imgName: '../img/common/sprite.png',
            cssName: '_sprite.scss',
            algorithm: 'top-down',
            padding: 24
        }));
    spriteData.img.pipe(gulp.dest('./common'));
    spriteData.css.pipe(gulp.dest('./scss/modules/'));
});

gulp.task('watch', function() {
    gulp.watch('./scss/**/*.scss',['compass'])
});