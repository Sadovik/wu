$( document ).ready(function() {

    $('.main-slider').carouFredSel({
        scroll		: {
            fx			: "crossfade",
            timeoutDuration	: 2000
        },
        prev	: {
            button	: ".main-prev",
            key		: "left"
        },
        next	: {
            button	: ".main-next",
            key		: "right"
        },
        pagination	: ".main-pagination"
    });

    $('.slider-clients').carouFredSel({
        items	: 4,
        scroll	: {
            items			: 1,
            duration		: 2000
        },
        prev	: {
            button	: ".client-prev",
            key		: "left"
        },
        next	: {
            button	: ".client-next",
            key		: "right"
        }
    });

    $('.contacts-slider').carouFredSel({
        scroll		: {
            fx			: "crossfade",
            timeoutDuration	: 6000
        },
        prev	: {
            button	: ".contact-prev",
            key		: "left"
        },
        next	: {
            button	: ".contact-next",
            key		: "right"
        }
    });

    $('.overview-slider').carouFredSel({
        scroll		: {
            fx			: "crossfade",
            timeoutDuration	: 6000
        },
        prev	: {
            button	: ".overview-prev",
            key		: "left"
        },
        next	: {
            button	: ".overview-next",
            key		: "right"
        }
    });

    var documentBody = $('body');

    function toggleOverlay(e) {
        e.preventDefault();
        e.stopPropagation();
        documentBody.toggleClass('portfolio-preview');
    }

    $(document).on('click', '.btn-more', function(e) {
        toggleOverlay(e);
    });

    $(document).on('click', '.overlay', function(e) {
        if ($(e.target).closest('.preview-wrapper').length == 0) {
            toggleOverlay(e);
        }
    });

    $(document).on('click', '.close', function(e) {
        toggleOverlay(e);
    });

});